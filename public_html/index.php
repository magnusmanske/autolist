<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');
ini_set('memory_limit','200M');
set_time_limit ( 60 * 10 ) ; // 10 min
ini_set('default_socket_timeout', 240);

require_once ( '/data/project/catscan2/public_html/omniscan.inc' ) ;
require_once ( '/data/project/pagepile/public_html/pagepile.php' ) ;

$start_time = microtime(true) ;

function escape_attr ( $s ) {
	return str_replace('"','&quot;',str_replace("'",'&#039;',$s)) ;
}

$lang = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$mode = get_request ( 'mode' , '' ) ;
$category = trim ( str_replace ( '_' , ' ' , ucfirst(trim(get_request ( 'category' , '' ))) ) ) ;
$depth = get_request ( 'depth' , 12 ) * 1 ;
$namespace = get_request ( 'namespace' , 0 ) * 1 ;
$manual_list = trim ( get_request ( 'manual_list' , '' ) ) ;
$wdq = trim ( str_replace ( '_' , ' ' , get_request ( 'wdq' , '' ) ) ) ;
$wdqs = trim ( get_request ( 'wdqs' , '' ) ) ;
$pagepile = get_request ( 'pagepile' , '' ) ;
$label_contains = trim ( get_request ( 'label_contains' , '' ) ) ;
$label_contains_not = trim ( get_request ( 'label_contains_not' , '' ) ) ;
$chunk_size = get_request ( 'chunk_size' , 10000 ) * 1 ;
$find = trim ( get_request ( 'find' , '' ) ) ;
$find_label = get_request ( 'find_label' , 0 ) * 1 ;
$find_alias = get_request ( 'find_alias' , 0 ) * 1 ;
$find_desc = get_request ( 'find_desc' , 0 ) * 1 ;
$find_langs = trim ( get_request ( 'find_langs' , '' ) ) ;
$format = trim ( get_request ( 'format' , 'default' ) ) ;
$download = isset($_REQUEST['download']) ;
if ( $format == 'download' ) $download = 1 ;

function out ( $s ) {
	global $download , $format ;
	if ( $download ) return ;
	if ( $format != 'default' ) return ;
	print $s ;
	myflush() ;
}


$wdq_logfile = '/data/project/autolist/wdq_log.tab' ;
if ( $wdq != '' ) {
	$ts = date ( 'Y-m-d' ) ;
	file_put_contents ( $wdq_logfile , "$ts\t$wdq\n" , FILE_APPEND ) ;
}


$mode_list = array(
	'manual' => 'Manual list' ,
	'cat' => 'Category' ,
	'wdq' => 'WDQ' ,
	'wdqs' => 'WDQS' ,
	'find' => 'Find'
) ;

$connectors = array('and','or','not') ;

$modes = array (
	'manual' => get_request('mode_manual','or') ,
	'cat' => get_request('mode_cat','or') ,
	'wdq' => get_request('mode_wdq','not') ,
	'wdqs' => get_request('mode_wdqs','or') ,
	'find' => get_request('mode_find','or')
) ;

if ( $pagepile_enabeled ) {
	$mode_list['pagepile'] = "PagePile" ;
	$modes['pagepile'] = get_request('mode_pagepile','or') ;
}

// Backwards compatability
if ( $mode == 'cat_no_wdq' ) { $modes['cat']='or'; $modes['wdq']='not'; }
if ( $mode == 'cat_and_wdq' ) { $modes['cat']='or'; $modes['wdq']='and'; }
if ( $mode == 'cat_or_wdq' ) { $modes['cat']='or'; $modes['wdq']='or'; }
if ( $mode == 'wdq_no_cat' ) { $modes['cat']='not'; $modes['wdq']='or'; }

$find_markers = $find_label + $find_alias + $find_desc ;
if ( $find != '' and $find_markers == 0 ) {
	$find_label = 1 ;
}
$find_markers = 0 ; // Was used to check for blank labels; takes too long, so deactivated.

$petscan_note3 = "<div style='margin:3px;padding:3px;text-align:center;background-color:#FF4848;font-size:12pt;'><b>This tool will be replaced with <a href='https://petscan.wmflabs.org/'>PetScan</a> in the near future!</b><br/>" ;
$petscan_note3 .= "This tool is becoming increasingly incompatible with the evolving technology on Labs, and will be deprecated soon.<br/>" ;
$petscan_note3 .= "PLEASE make an effort to convert WDQ queries into SPARQL, as WDQ is not available in PetScan.<br/>" ;
$petscan_note3 .= "All links to this tool with URL parameters will be forwarded to PetScan, and should remain functional.<br/>" ;
$petscan_note3 .= "Please add any blocking issues to the <a href='https://bitbucket.org/magnusmanske/petscan/issues?status=new&status=open'>PetScan bug tracker</a>.</div>" ;



out ( get_common_header('','AutoList 2') ) ;
out ( $petscan_note3 ) ;
$html = "<div><form method='post' class='form-inline' action='?'>
<table class='table table-condensed table-striped'><tbody>
<tr><th>Wiki</th><td><input type='text' value='$lang' name='language' class='span1' /> . <input type='text' value='$project' name='project' class='span2' /> . org</td>
<td rowspan='4'><div><b>Manual item list</b></div><textarea placeholder='Item list, one per line' rows=8 cols=15 stlye='max-width:100px' name='manual_list'>$manual_list</textarea></td>
</tr>
<tr><th>Category</th><td><input type='text' placeholder='Category (root) name' value='".escape_attr($category)."' name='category' class='span5' />
, depth <input type='number' value='".($depth*1)."' name='depth' class='span1' />
, NS <input type='number' value='$namespace' name='namespace' class='span1' />
</td></tr>
<tr><th>WDQ</th><td><input type='text' id='wdq' placeholder='WikiData Query' value='".escape_attr($wdq)."' name='wdq' class='span6' /> <a href='//wdq.wmflabs.org/api_documentation.html' target='_blank'>WDQ API documentation</a></td></tr>
<tr><th>WDQS</th><td><textarea style='width:50%' id='wdqs' name='wdqs' class='span6' placeholder='WDQS (sparql) query' rows=2>".$wdqs."</textarea>
<div style='display:inline'>
<span id='wdq2sparql' title='Convert WDQ to WDQS with one click!'><a href='#' onclick='wdq2sparql(\"#wdq\",\"#wdqs\");return false'>WDQ2WDQS</a></span>
<br/>SPARQL must <i>only</i> return one variable with result items!
</div>
</td></tr>
<tr><th>Find on<br/>Wikidata</th><td>
<input type='text' placeholder='Perfect match or SQL LIKE pattern' value='".escape_attr($find)."' name='find' class='span5' /> in 
<label><input type='checkbox' value='1' name='find_label' " . ($find_label?'checked ':'') . "/> Label</label>
<label><input type='checkbox' value='1' name='find_alias' " . ($find_alias?'checked ':'') . "/> Alias</label>
<label><input type='checkbox' value='1' name='find_desc' " . ($find_desc?'checked ':'') . "/> Description</label><br/>
Only in <input name='find_langs' value='".escape_attr($find_langs)."' placeholder='en,de,es,...' type='text' class='span4' />
</td></tr>" ;
if ( $pagepile_enabeled ) {
	$html .= "<tr><th>PagePile</th><td><input type='text' placeholder='PagePile ID' value='".escape_attr($pagepile)."' name='pagepile' class='span6' /> <a href='//tools.wmflabs.org/pagepile/' target='_blank'>PagePile documentation</a>" ;
	$html .= "<input type='hidden' name='pagepile_enabeled' value='1' /></td></tr>" ;
}
$html .= "
<tr><th>Mode</th><td>
<table class='table table-condensed table-striped'><thead><tr><th>Source</th>" ;

foreach ( $connectors AS $k ) $html .= "<th>" . strtoupper($k) . "</th>" ;
$html .= "</tr></thead><tbody>" ;

foreach ( $mode_list AS $k => $v ) {
	$html .= "<tr><th>$v</th>" ;
	foreach ( $connectors AS $v2 ) {
		$html .= "<td><input type='radio' name='mode_$k' value='$v2' " . ($modes[$k]==$v2?'checked ':'') . "/></td>" ;
	}
	$html .= "</tr>" ;
}


$html .= "</tbody></table>
<div style='font-size:9pt'>Items will be combined by OR, then subset by AND, then subset by NOT.</div>
</td></tr>
<tr><th>Label</th><td>
Contains <input type='text' name='label_contains' value='".escape_attr($label_contains)."' />
| does not contain <input type='text' name='label_contains_not' value='".escape_attr($label_contains_not)."' />
(works only as subsequent filter)
</td></tr>
<tr><td/><td>
Break results into chunks of <input type='number' name='chunk_size' class='span2' value='$chunk_size' />" ;

if ( $pagepile_enabeled ) {
	$html .= "<div>Format:" ;
	$html .= "<label><input type='radio' name='format' value='default' " . ($format=='default'?'checked':'') . " /> Default</label>" ;
	$html .= "<label><input type='radio' name='format' value='download' " . ($format=='download'?'checked':'') . " /> Download</label>" ;
	$html .= "<label><input type='radio' name='format' value='pagepile' " . ($format=='pagepile'?'checked':'') . " /> PagePile</label>" ;
	$html .= "</div>" ;
}

$html .= "<input type='submit' class='btn btn-primary' name='run' value='Run' />
<a id='permalink' href='#' style='font-size:14pt;display:none'>Permalink</a>
<a id='download_link' href='#' style='font-size:14pt;display:none'>Download</a>
<input type='hidden' name='statementlist' value='" . escape_attr(get_request('statementlist','')) . "' />
</td></tr>
</tbody></table>
</form></div>" ;
out ( $html ) ;

out ( "<div><i>Note: </i> This tool is currently throttled to one edit every 10 seconds, because it would otherwise flood Recent Changes on Wikidata.<br/>
Help me get <a href='https://phabricator.wikimedia.org/T66829' target='_blank'>this bug</a> resolved, so the throttling can be removed again!</div>" ) ;

out ( "<div id='control_wrapper' style='position:fixed;right:5px;top:100px;z-index:5;display:none' class='well'>
<div id='oauth_status'></div>
<h2>Controls</h2>
<div>Add/remove statements<br/><textarea id='statementlist' style='width:200px;height:100px;'>" ) ;

out ( get_request ( 'statementlist' , '' ) ) ;

out ( "</textarea>
<div style='font-size:8pt;line-height:110%'>
e.g. \"P31:Q5\" for \"instance of:human\"<br/>
Prefix with \"-\" to remove claim<br/>
One command per line
</div>
</div>
<div><button id='run_tag' class='btn btn-success' onclick='run_tag();return false'>Process commands</button>
<button style='display:none' id='stop_tag' class='btn btn-danger' onclick='stop_tag();return false'>EMERGENCY STOP</button></div>
<div id='control_status'></div>
</div>" ) ;

if ( !isset($_REQUEST['run']) or ( $category == '' and $wdq == '' and $wdqs == '' and $pagepile == '' and $manual_list == '' and ( $find == '' and $find_markers == 0 ) ) ) {
	out ( "<script>$('input[name=\"category\"]').focus();</script>" ) ;
	out ( get_common_footer() ) ;
	exit ( 0 ) ;
}

out ( "<hr/>" ) ;

$items = array() ;
$db_wd = openDB ( 'wikidata' , 'wikidata' ) ;

$data = (object) array() ;
foreach ( $modes AS $mode => $con ) $data->$mode = new Pagelist ;
//array('items'=>array(),'has_data'=>false) ;

//print "<pre>" ; print_r ( $data ) ; print "</pre>" ;


if ( $manual_list != '' ) {
	$data->manual->blankFile ( 'wikidata' , 'wikidata' ) ;
	$fh = fopen ( $data->manual->file , 'w' ) ;
	$manual_list = explode ( "\n" , $manual_list ) ;
	foreach ( $manual_list AS $i ) {
		$i = trim ( preg_replace ( '/\D/' , '' , $i ) ) ;
		if ( $i != '' ) fwrite ( $fh , "Q$i\t0\n" ) ;
	}
	fclose ( $fh ) ;
	out ( "<p>Using " . number_format($data->manual->wcl()) . " items from manual list</p>" ) ;
}

if ( $category != '' ) {
	out ( "<p>Getting pages in category tree... " ) ;
	$data->cat->loadCategoryTree ( array ( 'language'=>$lang , 'project'=>$project , 'depth'=>$depth , 'root'=>$category , 'namespace'=>$namespace , 'redirects'=>'none' ) ) ;
	out ( number_format($data->cat->wcl()) . " pages found.</p>" ) ;
	out ( "<p>Getting corresponding Wikidata items... " ) ;
	$data->cat = $data->cat->getWikidataItems() ;
	out ( number_format($data->cat->wcl()) . " items found.</p>" ) ;
}

if ( $wdq != '' ) {
	out ( "<p>Getting WDQ data... " ) ;
	$data->wdq->loadWDQ ( $wdq ) ;
	out ( number_format($data->wdq->wcl()) . " items loaded.</p>" ) ;
}

if ( $wdqs != '' ) {
	out ( "<p>Getting WDQS data... " ) ;
	$data->wdqs->loadWDQS ( $wdqs ) ;
	out ( number_format($data->wdqs->wcl()) . " items loaded.</p>" ) ;
}

if ( $pagepile != '' ) {
	out ( "<p>Getting PagePile data... " ) ;
	$data->pagepile->loadWikidataPagePile ( $pagepile ) ;
	out ( number_format($data->pagepile->wcl()) . " items loaded.</p>" ) ;
}

if ( $find != '' or $find_markers > 0 ) {
	out ( "<p>Finding string matches... " ) ;
	$find = $db_wd->real_escape_string ( mb_strtolower ( $find , 'UTF-8' ) ) ;

	$sql_lang = '' ;
	if ( $find_langs != '' ) {
		$a = explode ( ',' , $db_wd->real_escape_string ( $find_langs ) ) ;
		$sql_lang = " AND term_language IN (\"" . implode ( '","' , $a ) . '") ' ;
	}
	
	$a = array() ;
	if ( $find_label ) $a[] = '"label"' ;
	if ( $find_alias ) $a[] = '"alias"' ;
	if ( $find_desc ) $a[] = '"description"' ;
	$sql_lang .= " AND term_type IN (" . implode(',',$a) . ") " ;

	if ( $find == '' ) {
		
//		$sql = "select * from wb_entity_per_page WHERE epp_entity_type='item' and NOT EXISTS (SELECT * FROM wb_terms WHERE term_entity_type='item' and term_entity_id=epp_entity_id $sql_lang )" ;
		$sql = "select * from wb_entity_per_page WHERE epp_entity_type='item' and epp_entity_id NOT IN (SELECT DISTINCT term_entity_id FROM wb_terms WHERE term_entity_type='item' $sql_lang )" ;
		
	} else {
		$sql = "SELECT DISTINCT term_entity_id FROM wb_terms WHERE term_entity_type='item' $sql_lang " ;
//		$sql .= "AND term_search_key LIKE \"$find\"" ;
		$sql .= "AND term_search_key LIKE \"$find\"" ;
	}
	
//	print "<pre>" ; print_r ( $sql ) ; print "</pre>" ;
	
	$db_wd = openDB ( 'wikidata' , 'wikidata' ) ;
	if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db_wd->error . ']');
	$data->find->blankFile ( 'wikidata' , 'wikidata' ) ;
	$fh = fopen ( $data->find->file , 'w' ) ;
	while($o = $result->fetch_object()){
		fwrite ( $fh , 'Q'.$o->term_entity_id."\t0\n" ) ;
	}
	fclose ( $fh ) ;
	out ( number_format($data->find->wcl()) . " items loaded.</p>" ) ;
}



out ( "<p>Combining datasets... " ) ;


// Special case: Only one data source => "OR"
$last_mode = '' ;
$count = 0 ;
foreach ( $data AS $mode => $v ) {
	if ( !$v->hasData() ) continue ;
	$last_mode = $mode ;
	$count++ ;
}

if ( $count == 1 ) {
	out ( "<div>Only one dataset found; using '$last_mode'.</div>" ) ;
	$modes[$last_mode] = 'or' ;
}

$mode_counter = array() ;
foreach ( $connectors as $con ) $mode_counter[$con] = 0 ;
foreach ( $modes AS $mode => $con ) {
	if ( !$data->$mode->hasData() ) continue ;
	$mode_counter[$con]++ ;
}

//print "<pre>" ; print_r ( $mode_counter ) . "</pre>" ;

//$items = array() ;
$pl = new Pagelist ;
$pl->language = 'wikidata' ;
$pl->project = 'wikidata' ;
foreach ( $data AS $mode => $v ) {
	if ( !$v->hasData() ) continue ;
	if ( $modes[$mode] != 'or' ) continue ;
	$pl->union ( $pl , $v ) ;
	$v->cleanup() ;
}

out ( "<div>After OR : " . number_format($pl->wcl()) . " items.</div>" ) ;

if ( $mode_counter['and'] > 0 ) {

	$first = true ;
	foreach ( $data AS $mode => $v ) {
		if ( !$v->hasData() ) continue ;
		if ( $modes[$mode] != 'and' ) continue ;
		if ( $first and $mode_counter['or'] == 0 ) $pl->union ( $pl , $v ) ; // No prior data from OR; seed first AND set
		else $pl->subset ( $pl , $v ) ;
		$v->cleanup() ;
		$first = false ;
	}

	out ( "<div>After AND : " . number_format($pl->wcl()) . " items.</div>" ) ;
}


if ( $mode_counter['not'] > 0 ) {
	foreach ( $data AS $mode => $v ) {
		if ( !$v->hasData() ) continue ;
		if ( $modes[$mode] != 'not' ) continue ;
		$pl->only_in_list_1 ( $pl , $v ) ;
		$v->cleanup() ;
	}
	out ( "<div>After NOT : " . number_format($pl->wcl()) . " items.</div>" ) ;
}



out ( number_format($pl->wcl()) . " items in combination.</p>" ) ;


if ( $label_contains . $label_contains_not != '' ) {
	out ( "<p>Filtering +'$label_contains' / -'$label_contains_not' ... " ) ;
	$pl2 = new Pagelist ;
	$pl2->blankFile ( 'wikidata' , 'wikidata' ) ;
	$fh = fopen ( $pl2->file , 'w' ) ;
	
	while ( $a = $pl->getNextBatch() ) {
		$items = array() ;
		foreach ( $a AS $v ) $items[] = preg_replace('/\D/','',$v[0]) ;
		$sql = "SELECT DISTINCT term_entity_id FROM wb_terms t1 WHERE t1.term_type='label' AND t1.term_entity_type='item'" ;
		if ( $label_contains != '' ) $sql .= " AND t1.term_text LIKE '%".$db_wd->real_escape_string($label_contains)."%'" ;
		if ( $label_contains_not != '' ) $sql .= " AND NOT EXISTS (SELECT * FROM wb_terms t2 WHERE t2.term_type='label' AND t2.term_entity_type='item' AND t2.term_text LIKE '%".$db_wd->real_escape_string($label_contains_not)."%' AND t2.term_entity_id=t1.term_entity_id)" ;
		$sql .= " AND t1.term_entity_id IN (".implode(',',$items).")" ;
//print "<pre>$sql</pre>" ;
		unset ( $items ) ;
		$items = array() ;
		if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db->error . ']');
		unset ( $sql ) ;
		while($o = $result->fetch_object()){
			fwrite ( $fh , "Q".$o->term_entity_id."\t0\n" ) ;
		}
	}
	
	fclose ( $fh ) ;
	$pl = $pl2 ;
	out ( number_format($pl->wcl()) . " items remain</p>" ) ;
}

//$a = $pl->asRawArray() ; print "<pre>" ; print_r ( $a ) ; print "</pre>" ;

$mem = round(memory_get_peak_usage(true)/100000)/10 ;
$sec = microtime(true) - $start_time ;
out ( "<p>Query took {$sec} seconds. {$mem} MB memory used.</p>" ) ;



if ( $format == 'pagepile' ) {
	$pp = new PagePile ;
	$pp->createNewPile ( 'wikidatawiki' , '' ) ;
	while ( $a = $pl->getNextBatch() ) {
		foreach ( $a AS $v ) $pp->addPage ( $v[0] , 0 ) ;
	}
	$pp->setTrackFromUrl ( 'AutoList 2' ) ;
	$pp->printAndEnd() ;
}

if ( $download ) {
	header ( "Content-type: text/plain\n" ) ;
	while ( $a = $pl->getNextBatch() ) {
		foreach ( $a AS $v ) print $v[0]."\n" ;
	}
//	foreach ( $items AS $i ) print "Q$i\n" ;
	exit ( 0 ) ;
}


print "<script>
var wp_lang = '$lang' ;
var chunks = [" ;

$first = true ;
$pl->batch_size = $chunk_size ;
while ( $a = $pl->getNextBatch() ) {
	if ( !$first ) print ',' ;
	print '[' ;
	foreach ( $a AS $k => $v ) {
		if ( $k > 0 ) print ',' ;
		print preg_replace('/\D/','',$v[0]) ;
	}
	print ']' ;
	$first = false ;
}

/*
$chunks = array_chunk ( $items , $chunk_size ) ;
foreach ( $chunks AS $k => $v ) {
	if ( $k > 0 ) print "," ;
	print "[" ;
	print implode(',',$v) ;
	print "]" ;
}
*/

print "] ;
var current_chunk = 0 ;
var chunk_size = $chunk_size ;
var _namespace = $namespace ;
</script>" ;


?>

<hr/><div>
<div id='chunk_management' style='float:right'></div>
Checkboxes :
<button class='btn btn-info' onclick='set_cb("on");return false'>check</button>
<button class='btn btn-info' onclick='set_cb("off");return false'>uncheck</button>
<button class='btn btn-info' onclick='set_cb("toggle");return false'>toggle</button>
</div>

<hr/>
<div style='max-height:500px;overflow:auto' id='table_div'></div>

<hr/>


<script>

var max_widar_concurrent = 1 ;
var widar_edit_delay = 10*1000 ; // ms
var widar_api = '/widar/index.php?' ;
var do_stop = false ;
var done = 0 ;
var qlist = [] ;
var widar_running ;

function add2permalink ( name , url ) {
	var text = $('input[name='+name+']').val() ;
	if ( text == '' ) return ;
	url.push ( name+'=' + encodeURIComponent(text) ) ;
}

function add2permalink_cb ( name , url ) {
	var v = $('input[name='+name+']:checked').val() ;
	if ( typeof v == 'undefined' ) return ;
	url.push ( name+'=' + v ) ;
}

function updatePermalink () {
	var url = [] ;
	$.each ( ['language','project','category','depth','wdq','pagepile'] , function ( k , v ) {
		url.push ( v+"=" + encodeURIComponent ( $('input[name="'+v+'"]').val() ) ) ;
	} ) ;
	$.each ( ['wdqs'] , function ( k , v ) {
		url.push ( v+"=" + encodeURIComponent ( $('textarea[name="'+v+'"]').val() ) ) ;
	} ) ;
	
	add2permalink_cb ( 'mode' , url ) ;
	url.push ( 'statementlist=' + encodeURIComponent($('#statementlist').val()) ) ;
	url.push ( 'run=Run' ) ;

	add2permalink ( 'label_contains' , url ) ;
	add2permalink ( 'label_contains_not' , url ) ;

	add2permalink ( 'find' , url ) ;
	add2permalink ( 'find_langs' , url ) ;
	add2permalink_cb ( 'find_label' , url ) ;
	add2permalink_cb ( 'find_alias' , url ) ;
	add2permalink_cb ( 'find_desc' , url ) ;

	add2permalink_cb ( 'mode_manual' , url ) ;
	add2permalink_cb ( 'mode_cat' , url ) ;
	add2permalink_cb ( 'mode_wdq' , url ) ;
	add2permalink_cb ( 'mode_wdqs' , url ) ;
	add2permalink_cb ( 'mode_find' , url ) ;
	
	
	url.push ( 'chunk_size=' + chunk_size ) ;
	if ( _namespace != 0 ) url.push ( 'namespace=' + _namespace ) ;
	url = "?" + url.join("&") ;
	$('#permalink').show().attr({href:url}) ;
	
	url += "&download=1" ;
	$('#download_link').show().attr({href:url}) ;
	
	$('input[name=statementlist]').val($('#statementlist').val());
}

function stopRun (s) {
	$('#run_tag').show() ;
	$('#stop_tag').hide() ;
	$('#control_status').html ( s ) ;
	$("input.item_cb").prop('disabled', false) ;
}

function doWidar ( q , todo ) {
	if ( todo.length == 0 ) {
		widar_running-- ;
		$('tr[q="'+q+'"]').hide().find('input').remove() ;
		$('#table_div').scroll();
		done++ ;
		$('#control_status').html ( done + " of " + (done+qlist.length) + " done, " + qlist.length + " to go" ) ;
		return tag_next_one() ;
	}
	
	var o = todo.shift() ;
	var params = { botmode:1 } ;
	
	function nextWiDaR () {
		if ( widar_edit_delay == 1 ) {
			doWidar ( q , todo ) ;
		} else {
			setTimeout ( function () { doWidar ( q , todo ) } , widar_edit_delay ) ;
		}
	}

	function runWiDaR () {
		params.tool_hashtag = 'autolist2' ;
		$.get ( widar_api , params , function ( d ) {
	//		console.log ( d ) ;
			if ( d.error != 'OK' ) {
				if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Nonce already used/) ) {
					$('#control_status').html ( "WARNING: " + d.error ) ;
					setTimeout ( runWiDaR , 500 ) ;
				} else {
					return stopRun ( "<span style='color:red'>WiDaR error: " + d.error + "</span>" ) ;
				}
			} else {
				nextWiDaR() ;
			}
		} , 'json' ) .fail(function() {
			console.log ( "WIDAR JSON fail for " + q + "!" ) ;
			console.log ( o ) ;
			todo.unshift ( o ) ;
			nextWiDaR() ;
		} ) ;
	}
	
	if ( o.mode == 'itemlink' ) {
		params.action = 'set_claims' ;
		params.ids = 'Q'+(''+q).replace(/\D/g,'') ;
		params.prop = 'P'+(''+o.prop).replace(/\D/g,'') ;
		params.target = 'Q'+(''+o.target).replace(/\D/g,'') ;
		runWiDaR() ;
	} else if ( o.mode == 'removeclaim' ) {
		var ids = 'Q'+(''+q).replace(/\D/g,'') ;
		var prop = 'P'+(''+o.prop).replace(/\D/g,'') ;
		var target = 'Q'+(''+o.target).replace(/\D/g,'') ;
		
		$.getJSON ( '//www.wikidata.org/w/api.php?callback=?' , {
			action:'wbgetentities',
			ids:ids,
			format:'json',
			props:'claims|info'
		} , function ( d ) {
			var claims = ((((d.entities||{})[ids]||{}).claims||{})[prop]) ;
			if ( typeof claims == 'undefined' ) {
				console.log ( ids + " has no claims for " + prop ) ;
				nextWiDaR() ;
				return ;
			}
			var statement_id ;
			$.each ( (claims||[]) , function ( k , v ) {
				if ( target == 'Q' ) { // No specific target, first claim will do
					statement_id = v.id ;
					return false ;
				}
				var nid = (((((v||{}).mainsnak||{}).datavalue||{}).value||{})['numeric-id']) ;
				if ( typeof nid == 'undefined' ) return ;
				nid = 'Q' + nid ;
				if ( nid != target ) return ; // Correct property, wrong target
				statement_id = v.id ;
				return false ; // Got one
			} ) ;
			if ( typeof statement_id == 'undefined' ) {
				console.log ( prop + " exists for " + ids + ", but no target " + target ) ;
				nextWiDaR() ;
				return ;
			}
			params.action = 'remove_claim' ;
			params.id = statement_id ;
			params.baserev = d.entities[ids].lastrevid ;
//			console.log ( params ) ;
			runWiDaR() ;
		} ) ;
	} else {
		// TODO
		console.log ( "Unknown mode " + o.mode ) ;
		widar_running-- ;
		return ;
	}
	
	
	
}

function tag_next_one () {
	
	if ( do_stop ) {
		if ( widar_running > 0 ) return ;
		return stopRun("<span style='color:red'>User stopped run.</span>") ;
	}
	if ( qlist.length == 0 ) {
		if ( widar_running > 0 ) return ;
		return stopRun("<span style='color:green'>All done!</span><br/><small>(WDQ may take ~15 min to update)</small>") ;
	}

	var q = qlist.shift() ;	
	var todo = [] ;
	var rows = $('#statementlist').val().split("\n") ;
	$.each ( rows , function ( dummy , row ) {
		var removing = $.trim(row).substr(0,1) == '-' ;
		row = row.split(":") ;
		if ( row.length != 2 && !removing ) return ;
		var o = { mode:'itemlink' } ;
		if ( removing ) {
			o.mode = 'removeclaim' ;
			row[0] = $.trim(row[0].substr(1)) ;
		} else {
		}
		o.prop = row[0].replace(/\D/g,'') ;
		o.target = (row[1]||'').replace(/\D/g,'') ;
		if ( o.prop == '' || ( o.target == '' && !removing) ) return ; // Bad row
		todo.push ( o ) ;
	} ) ;
//	console.log ( todo ) ;
	if ( todo.length == 0 ) {
		return stopRun ( "<span style='color:red'>Put something in the box above!</span>" ) ;
	}
	
	widar_running++ ;
	doWidar ( q , todo ) ;
	
	if ( widar_running < max_widar_concurrent ) tag_next_one() ;
}

function run_tag () {
	$('#run_tag').hide() ;
	$('#stop_tag').show() ;
	do_stop = false ;
	done = 0 ;
	widar_running = 0 ;
	qlist = [] ;
	$("input.item_cb").each ( function(){
		o = $(this) ;
		if ( o.prop("checked") ) qlist.push ( $(o.parents('tr')[0]).attr('q') ) ;
	} ) ;
	$("input.item_cb").prop('disabled', true) ;
//	console.log ( qlist ) ;
	tag_next_one() ;
}

function stop_tag () {
	do_stop = true ;
}

$.fn.is_on_screen = function(){
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
 
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
 
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};

var main_langs = [wp_lang,'en','de','es','it','fr'] ;

function loadItem ( o ) {
	if ( o.css('display') === 'none' ) return ; // Hacking around Chrome bug; should be: if ( !img.is(':visible') ) return ;
	if ( !o.is_on_screen() ) return ;
	o.removeClass('itemrow_unloaded') ;
	var q = o.attr('q') ;

//	var url = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids=Q"+q+"&props=labels|descriptions&callback=?" ;
//	console.log ( url ) ;
	
	$.getJSON ( '//www.wikidata.org/w/api.php?callback=?' , {
		action:'wbgetentities',
		ids:'Q'+q,
		format:'json',
		props:'labels|descriptions'
	} ,	function ( d ) {
	console.log ( o.attr('q') ) ;
		var e = (d.entities['Q'+q]||{}) ;
		var langs = (e.labels||{}) ;
		var descs = (e.descriptions||{}) ;
		var title = 'Q'+q ;
		var desc = '' ;
		$.each ( main_langs , function ( dummy , l ) { if ( typeof langs[l] != 'undefined' ) { title=langs[l].value ; return false }  } ) ;
		$.each ( main_langs , function ( dummy , l ) { if ( typeof descs[l] != 'undefined' ) { desc=descs[l].value ; return false }  } ) ;
		o.find('a.itemlink').text ( title ) ;
		o.find('td.itemdesc').text ( desc ) ;
	} ) ;
  
}

function set_cb ( m ) {
	$("input.item_cb").each ( function(){
		o = $(this) ;
		if ( m == 'on' ) o.prop("checked",true) ;
		else if ( m == 'off' ) o.prop("checked",false) ;
		else if ( m == 'toggle' ) o.prop("checked",!o.prop("checked")) ;
	} ) ;
}

function checkOauthStatus () {
	$.get ( widar_api , {
		action:'get_rights',
		botmode:1
	} , function ( d ) {
		var h = '' ;
		if ( d.error != 'OK' || typeof (d.result||{}).error != 'undefined' ) {
			h += "<div><a title='You need to authorise WiDaR to edit on your behalf if you want this tool to edit Wikidata.' target='_blank' href='/widar/index.php?action=authorize'>WiDaR</a><br/>not authorised.</div>" ;
		} else {
			h += "<div>Logged into <a title='WiDaR authorised' target='_blank' href='/widar/'>WiDaR</a> as " + d.result.query.userinfo.name + "</div>" ;
			$.each ( d.result.query.userinfo.groups , function ( k , v ) {
				if ( v != 'bot' ) return ;
				h += "<div><b>You are a bot</b>, no throttling for you!</div>" ;
			} ) ;
		}
		$('#oauth_status').html ( h ) ;
		$('#oauth_status a').tooltip({placement:'left'}) ;
		$.each ( (((((d||{}).result||{}).query||{}).userinfo||{}).groups||[]) , function ( k , v ) {
			if ( v == 'bot' ) {
				max_widar_concurrent = 5 ;
				widar_edit_delay = 1 ;
			}
		} ) ;
	} , 'json' ) ;
}

function renderCurrentChunk () {
	var h = "<table class='table table-condensed table-striped' id='main_table'>" ; //  style='display:none'
	h += "<thead><th colspan=3 style='width:400px'>Item</th><th style='width:500px'>Description</th></thead><tbody>" ;

	$.each ( chunks[current_chunk] , function ( row , q ) {
		h += "<tr class='itemrow itemrow_unloaded' q='"+q+"'><th style='text-align:right;font-family:Courier'>"+(row+1)+"</th>" ;
		h += "<td><input type='checkbox' class='item_cb' checked /></td>" ;
		h += "<td><a class='itemlink' title='Q"+q+"' target='_blank' href='//www.wikidata.org/wiki/Q"+q+"'>Q"+q+"</a>" ;
		h += " <a title='Reasonator' href='/reasonator/?q="+q+"' target='_blank'><img src='//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png' border=0 /></a>" ;
		h += "</td>" ;
		h += "<td class='itemdesc'><small><i>...</i></small></td>" ;
		h += "</tr>" ;
	} ) ;
	h += "</tbody></table>" ;
	
	$('#table_div').html ( h ) ;
//	$('#main_table').show() ;
	setTimeout ( function () { $('#table_div').scroll() } , 100 ) ;
}

function initChunks() {
	var h = "Chunk " ;
	h += "<select class='span1' id='current_chunk'>" ;
	for ( var c = 0 ; c < chunks.length ; c++ ) {
		h += "<option value='" + c + "'" ;
		if ( c == current_chunk ) h += " selected" ;
		h += ">" + (c+1) + "</option>" ;
	}
	h += "</select>" ;
	h += " of " + chunks.length ;
	$('#chunk_management').html ( h ) ;
	$('#current_chunk').change ( function () {
		var c = $('#current_chunk option:selected').val() ;
		if ( current_chunk == c ) return ; // Already showing that chunk
		current_chunk = c ;
		console.log ( current_chunk ) ;
		renderCurrentChunk() ;
	} ) ;
}

$(document).ready ( function () {
	$('#table_div').scroll(function(){ $('tr.itemrow_unloaded').each ( function () { loadItem($(this)); } ) ; } ) ;
	initChunks() ;
	renderCurrentChunk() ;
	$('#control_wrapper').show() ;
	checkOauthStatus() ;
	$('#statementlist').keyup ( updatePermalink ) ;
	$('#table_div').mouseenter ( function () { $('#table_div').scroll() } ) ;
	updatePermalink() ;
} ) ;
</script>







<?PHP
print get_common_footer() ;
?>