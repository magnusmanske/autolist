var wd_autolist = {
	api : '//wdq.wmflabs.org/api' ,
	query : '' ,
	offset : 0 ,
	listlen : 50 ,
	show_props : '' ,
	loaded : false ,
	
	init : function () {
		var self = this ;
		self.wd = new WikiData () ;
	}  ,
	
	escape_attr : function ( t ) {
		if ( typeof ( t ) == 'undefined' ) return '' ;
		return t.replace ( /'/g , '&#39;' ) ;
	} ,
	
	setLanguage : function ( l ) {
		var self = this ;
		if ( self.wd.language == l ) return ; // Nothing to do
		self.wd.language = l ;
		self.wd.main_languages.unshift ( l ) ;
		if ( wd_auto_desc.lang != l ) {
			wd_auto_desc.lang = l ;
			wd_auto_desc.cache = {} ;
		}
		self.show() ;
	} ,
	
	runCatQuery : function ( cat_name , cat_lang , cat_project , cat_depth , callback ) {
		var self = this ;
		$('#loading').html("Running query...").show() ;
		var params = {
			lang : cat_lang ,
			project : cat_project ,
			cats : cat_name ,
			ns : 0 ,
			depth : cat_depth ,
			max : 30000,//self.mode=='either'?30000:90000 ,
			start : 0 ,
			wikidata : 1 ,
			format : 'json'
		} ;
		console.log ( params ) ;
		$.getJSON ( '/quick-intersection/?' , params , function ( d ) {
			self.d = {
				status : { error : 'OK' , items : 0 } ,
				items : []
			} ;
			$.each ( d.pages , function ( k , v ) {
				self.d.status.items++ ;
				if ( v.q !== undefined ) self.d.items.push ( v.q.replace(/\D/g,'') ) ;
			} ) ;
			console.log ( 'callback' ) ;
			if ( callback !== undefined ) { callback('cat'); return }
			$('#il_addon').html('('+self.d.status.items+' pages with '+self.d.items.length+' items)') ;
			$('#loading').hide() ;
			self.loaded = true ;
			self.show() ;
		} ) ;
	} ,

	runQuery : function ( query , callback ) {
		var self = this ;
		self.query = $.trim ( query ) ;
//		self.offset = 0 ;
		$('#nav_top').hide() ;
		$('#results').hide() ;
		$('#nav_bottom').hide() ;
		$('#il_addon').html('') ;
		if ( self.query == '' ) return ;
		$('#loading').html("Running query...").show() ;
		$.get ( './log.php' , { wdq:query } , function(){} ) ;
		$.getJSON ( self.api+'?callback=?' , {
			q : self.query
		} , function ( d ) {
			self.d = d ;
			if ( callback !== undefined ) { callback('query'); return }
			if ( d.status.error != 'OK' ) {
				return ;
			}
			self.loaded = true ;
			$('#loading').hide() ;
			$('#il_addon').html('('+self.d.status.items+' items)') ;
			self.show() ;
		} ) ;
	} ,
	
	updatePermalink : function () {
		var self = this ;
		var href = 'http://tools.wmflabs.org/autolist/autolist1.html?' ;
		if ( self.wd.language != 'en' ) href += 'lang=' + self.wd.language + '&' ;
		if ( self.offset != 0 ) href += 'start=' + self.offset + '&' ;
		if ( self.listlen != 50 ) href += 'listlen=' + self.listlen + '&' ;
		if ( self.show_props != '' ) href += 'props=' + self.show_props + '&' ;
		if ( self.mode != 'either' ) href += 'mode=' + self.mode + '&' ;

		var cat_name = $('#cat_name').val() ;
		var cat_lang = $('#cat_lang').val() ;
		var cat_project = $('#cat_project').val() ;
		var cat_depth = $('#cat_depth').val() ;
		
		
		if ( cat_name != '' || self.mode != 'either' ) {
			href += "cat_name=" + escape(cat_name) + "&cat_lang=" + cat_lang + "&cat_project=" + cat_project + "&cat_depth=" + cat_depth + "&" ;
		}
		
		if ( cat_name == '' || self.mode != 'either' ) {
			href += 'q=' + escape ( self.query ) ;
		}
		$('#permalink').attr({'href':href}) ;
		$('#twitter').attr({'href':'http://twitter.com/intent/tweet?url='+escape(href)}) ;

		$('#email').attr({'href':'mailto:?to=&subject=Wikipedia/Wikidata item list&body='+escape(href)}) ;
		
		var embed = "<iframe style='border:1px solid black' width='940' height='300' src='"+href+"&nochrome=1"+"'></iframe>" ;
		$('#embed_code').val ( embed ) ;

		if ( self.query != '' && cat_name == '' ) {
			href = 'http://tools.wmflabs.org/autolist/download_query.php?' ;
			if ( self.wd.language != 'en' ) href += 'lang=' + self.wd.language + '&' ;
			href += 'q=' + escape ( self.query ) ;
			$('#download').attr({href:href}).show() ;
		} else $('#download').hide() ;

		
		$('#links').show() ;
	} ,
	
	getNavbar : function () {
		var self = this ;
		var h = [] ;
		var max = self.d.items.length ; //self.d.status.items ;
		for ( var i = 0 ; i < max ; i += self.listlen ) {
			var from = i+1 ;
			var to = i+self.listlen ;
			if ( to > max ) to = max ;
			if ( i == self.offset ) {
				h.push ( "<b>" + from + "-" + to + "</b>" ) ;
			} else {
				h.push ( "<a href='#' onclick='wd_autolist.seek("+i+")'>" + from + "-" + to + "</a>" ) ;
			}
		}
		h = "<div style='max-height:40px;overflow:auto'>" + h.join ( ' | ' ) + "</div>" ;
		return h ;
	} ,
	
	seek : function ( pos ) {
		var self = this ;
		self.offset = pos ;
		self.show() ;
		return false
	} ,
	
	show : function () {
		var self = this ;
		if ( !self.loaded ) return ;
		var items = [] ;
		var h = "<table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th>#</th><th class='oauth'></th><th>Item</th><th>Description</th><th>Wikipedia(s)</th>" ;
		if ( self.show_props != '' ) h += "<th>Props</th>" ;
		h += "</tr></thead><tbody>" ;
		
		for ( var i = 0 ; i < self.listlen && i+self.offset < self.d.items.length ; i++ ) {
			var q = self.d.items[i+self.offset] ;
			items.push ( 'Q'+q ) ;
			h += "<tr id='Q" + q + "'>" ;
			h += "<th>"+(i+self.offset+1)+"</th><td><a target='_blank' href='//www.wikidata.org/wiki/Q" + q + "'>Q" + q + "</a></td><td colspan=3 />" ;
			h += "</tr>" ;
		}
		h += '</tbody></table>' ;
		
		var navbar = self.getNavbar() ;
		
		$('#nav_top').html(navbar).show() ;
		$('#results').html(h).show() ;
		$('#nav_bottom').html(navbar).show() ;
		
		var l = self.wd.language ;
		$('#loading').html("Getting labels...").show() ;
		self.wd.loadItems ( items , { finished : function () {
			$.each ( items , function ( dummy , q ) {
				var i = self.wd.getItem ( q ) ;
				
				if ( i.getLabel() === undefined ) {
					$('#'+q).html ( "<th><td colspan='3'><i>Item has been deleted</i></td></th>" ) ;
					return ;
				}
				
				var desc = i.getDesc() ;
				var h = '' ;
				h += "<th>" + (dummy+self.offset+1) + "</th>" ;
				h += "<td class='oauth'><input type='checkbox' q='" + q + "' /></td>" ;
				h += "<td>" + i.getLink ( { target:'_blank' } ) ;
				h += "</td>" ;
				h += "<td><small id='desc_"+q+"'>" + (desc==''?'<i>generating...</i>':desc) + "</small></td>" ;
				var links = i.getWikiLinks() ;
				
				h += "<td>" ;
				h += "<div style='white-space: nowrap;'>" ;
				if ( undefined === links[l+'wiki'] ) {
					h += "<a target='_blank' style='color:red !important' href='//" + l + ".wikipedia.org/w/index.php?title=" + self.escape_attr(i.getLabel()) + "&action=edit'>" + l + ".wikipedia</a>" ;
				} else {
					h += "<a target='_blank' href='//" + l + ".wikipedia.org/wiki/" + self.escape_attr(links[l+'wiki'].title) + "'>" + l + ".wikipedia</a>" ;// (" + links[l+'wiki'].title + ")" ;
				}
				h += "&nbsp;<a title='Reasonator' href='//tools.wmflabs.org/reasonator/?q="+q+"&lang="+l+"' target='_blank'><img border=0 src='//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png'/></a>" ;
				h += "</div>" ;
				
				var other = [] ;
				$.each ( links , function ( lang , v ) {
					if ( lang == l+'wiki' ) return ;
					var m = lang.match ( /(.+)wiki$/ ) ;
					if ( m == null ) {
						other.push ( lang ) ;
						return ;
					}
					other.push ( "<a target='_blank' href='//"+m[1]+".wikipedia.org/wiki/"+self.escape_attr(v.title)+"'>"+m[1]+"</a>" ) ;
				} ) ;
				h += "<div style='max-width:400px;font-size:9pt'>" + other.join(', ') + "</div>" ;
				h += "</td>" ;
				
				if ( self.show_props != '' ) {
					h += "<td nowrap>" ;
					$.each ( self.show_props.split(',') , function ( k , prop ) {
						if ( !i.hasClaims(prop) ) return ;
						$.each ( i.getClaimsForProperty(prop) , function ( k2 , v2 ) {
							h += "<div title='P"+prop+"'>" ;
							if ( v2.mainsnak !== undefined && v2.mainsnak.datavalue !== undefined ) {
								var v = v2.mainsnak.datavalue ;
								if (v.type == 'time' ) {
									h += v.value.time.substr(8,10) ;
									if ( v.value.time.substr(0,1)=='-' ) h += "BCE" ;
								} else if (v.type == 'string' ) {
									h += v.value ;
								} else if ( v.type == 'globecoordinate' ) {
									h += Math.floor(v.value.latitude*1000)/1000 + '/' + Math.floor(v.value.longitude*1000)/1000 ;
								} else if (v.type == 'wikibase-entityid' ) {
									h += "<a target='_blank' href='//www.wikidata.org/wiki/Q" + v.value['numeric-id'] + "'>Q" + v.value['numeric-id'] + "</a>" ;
								} else console.log ( v2 ) ;
							} else console.log ( v2 ) ;
							h += "</div>" ;
						} ) ;
					} ) ;
					h += "</td>" ;
				}
				
				$('#'+q).html ( h ) ;


				if ( desc == '' ) {
					wd_auto_desc.loadItem ( q , {
						target:$('#desc_'+q) ,
			//			callback : function ( q , html , opt ) { console.log ( q + ' : ' + html ) } ,
			//			links : 'wikidata' , // wikipedia
						linktarget : '_blank'
					} ) ;
				}


			} ) ;
			$('#loading').hide() ;
			if ( $('#cb_has_oauth').is(':checked') ) $('.oauth').show() ;
		} } ) ;
		self.updatePermalink() ;
	} ,
	
	claim_it : function ( e ) {
		var self = wd_autolist ;
		var ids = [] ;
		$('td.oauth input').each ( function ( k , v ) {
			if ( !$(v).is(':checked') ) return ;
			ids.push ( $(v).attr('q') ) ;
		} ) ;

		function doit () {
			var prop = 'P' + $('#claim_dialog_prop').val().replace(/\D/g,'') ;
			var target = 'Q' + $('#claim_dialog_target').val().replace(/\D/g,'') ;
			
			if ( prop == '' || target == '' ) {
				$('#claim_dialog button.close').click() ;
				return false ;
			}

			$.cookie('autolist_last_prop', prop , { expires: 90, path: '/' });
			$.cookie('autolist_last_target', target , { expires: 90, path: '/' });
			
			var url = "http://tools.wmflabs.org/widar/index.php?action=set_claims&ids="+ids.join(",")+"&prop="+prop+"&target="+target ;
			url += '&tool_hashtag=autolist1' ;
			window.open(url);//,'WiDaR');
			$.each ( ids , function ( k , v ) {
				v = v.replace(/\D/g,'') * 1 ;
				self.d.items = $.grep ( self.d.items , function ( value ) {
					return value != v ;
				} ) ;
			} ) ;
			self.show() ;
			$('#claim_dialog button.close').click() ;
			return false ;
		}
		
		$('#claim_dialog_button_ok').unbind('click');
		$('#claim_dialog_button_ok').click ( doit ) ;
		$('#claim_dialog').modal( {
			shown : function () {
				if ( $.cookie('autolist_last_prop') !== undefined ) $('#claim_dialog_prop').val ( $.cookie('autolist_last_prop') ) ;
				if ( $.cookie('autolist_last_target') !== undefined ) $('#claim_dialog_target').val ( $.cookie('autolist_last_target') ) ;
				$('#claim_dialog_prop').focus() ;
			}
		} ) ;
		
		return false ;
	} ,
	
	run : function ( what ) {
		var self = this ;

//		console.log ( self.mode + " / " + what ) ;

		if ( self.mode == 'either' ) {
			if ( what == 'query' ) {
				self.runQuery ( $('#query').val() ) ;
			} else { // Cat
				self.runCatQuery ( $('#cat_name').val() , $('#cat_lang').val() , $('#cat_project').val() , $('#cat_depth').val() ) ;
			}
		} else if ( self.mode == 'and' || self.mode == 'or' ) {
		
			var cnt = 0 ;
			
			function cb ( done ) {
				console.log ( "Done: " + done + " (" + cnt + ")" ) ;
				self.sub_d.push ( self.d ) ;
				self.d = { error:'OK' , items:[] } ;
				cnt++ ;
				if ( cnt < 2 ) return ;
//				if ( self.sub_d.length < 2 ) return ;

				if ( self.mode == 'and' ) {
					var subset = {} ;
					$.each ( self.sub_d[0].items , function ( k , v ) { subset[v] = 1 } ) ;
					$.each ( self.sub_d[1].items , function ( k , v ) {
						if ( subset[v] ) self.d.items.push ( v ) ;
					} ) ;
				} else if ( self.mode == 'or' ) {
					var superset = {} ;
					$.each ( self.sub_d[0].items , function ( k , v ) { superset[v] = 1 } ) ;
					$.each ( self.sub_d[1].items , function ( k , v ) { superset[v] = 1 } ) ;
					$.each ( superset , function ( k , v ) { self.d.items.push ( k ) } ) ;
				}
//				console.log ( "1: " + self.sub_d[0].items.length ) ;
//				console.log ( "2: " + self.sub_d[1].items.length ) ;
//				console.log ( "Subset: " + self.d.items.length ) ;
				self.sub_d = {} ;
				$('#loading').hide() ;
				self.loaded = true ;
				self.show() ;
			}
		
			self.sub_d = [] ;
			self.runQuery ( $('#query').val() , cb ) ;
			self.runCatQuery ( $('#cat_name').val() , $('#cat_lang').val() , $('#cat_project').val() , $('#cat_depth').val() , cb ) ;
		
		} else {
			alert ( "Don't know mode '" + self.mode + "'" ) ;
			return ;
		}

	} ,
	
	
	the_end : ''
}


$(document).ready ( function () {
/*	if ( parent.location.protocol == 'https:' ) {
		window.location = 'http://tools.wmflabs.org/wikidata-todo/autolist.html' ; // Force http
		return ;
	}
*/	
	
	function updateLanguage () {
		wd_autolist.setLanguage ( $('#language').val() ) ;
	}
	
	function updateProps () {
		wd_autolist.show_props = $('#props').val() ;
		wd_autolist.show() ;
	}
	
	function param( name ) {
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( window.location.href );
	  if( results == null )
		return "";
	  else
		return unescape(results[1]);
	}
	
	$('#cat_depth').tooltip();
	$('#claim_it').tooltip();
	$('#widar').tooltip();
	
	// OAuth
	$('#cb_has_oauth').change ( function () {
		$('.oauth').toggle() ;
		$.cookie('widar_oauth', $('#cb_has_oauth').is(':checked')?1:0 , { expires: 90, path: '/' });
	} ) ;
	$('#claim_it').click ( wd_autolist.claim_it ) ;
	if ( $.cookie('widar_oauth') == 1 ) {
		$('#cb_has_oauth').prop('checked',true) ;
		$('#cb_has_oauth').change();
	}
	
	var cat_name = param('cat_name') ;
	var cat_lang = param('cat_lang') ;
	var cat_project = param('cat_project') ;
	var cat_depth = param('cat_depth') ;
	$('#cat_name').val ( cat_name ) ;
	$('#cat_lang').val ( cat_lang==''?'en':cat_lang ) ;
	$('#cat_project').val ( cat_project==''?'wikipedia':cat_project ) ;
	$('#cat_depth').val ( cat_depth==''?'12':cat_depth ) ;
	$('#cat_run').click ( function () {
		self.offset = 0 ;
		wd_autolist.run ( 'cat' ) ;
	} ) ;
	
	wd_autolist.mode = param('mode') ;
	if ( wd_autolist.mode == '' ) wd_autolist.mode = 'either' ;
	
	$('input[name=mode]').change ( function () {
		wd_autolist.mode = $('input[name=mode]:checked').val() ;
//		wd_autolist.updatePermalink() ;
	} ) ;
	$('input[name=mode][value='+wd_autolist.mode+']')[0].checked = true;

	var query = param('q') ;
	$('#query').val ( query ) ;
	var l = param('lang') ;
	if ( l != '' ) $('#language').val ( l ) ;
	wd_autolist.show_props = param('props') ;
	$('#props').val ( wd_autolist.show_props ) ;
	$('#main_form').submit ( function ( e ) { return false } ) ;
	wd_autolist.init() ;
	$('#language_update').click ( updateLanguage ) ;
	$('#props_update').click ( updateProps ) ;
	$('#run').click ( function () {
		self.offset = 0 ;
		wd_autolist.run ( 'query' ) ;
	} ) ;
	updateLanguage() ;
	if ( query != '' ) {
		wd_autolist.offset = param('start')*1 ;
		if ( param('listlen') != '' ) wd_autolist.listlen = param('listlen')*1 ;
		wd_autolist.run ( 'query' ) ;
//		wd_autolist.runQuery ( query ) ;
	} else if ( cat_name != '' ) {
		wd_autolist.offset = param('start')*1 ;
		if ( param('listlen') != '' ) wd_autolist.listlen = param('listlen')*1 ;
		wd_autolist.run ( 'cat' ) ;
//		wd_autolist.runCatQuery ( $('#cat_name').val() , $('#cat_lang').val() , $('#cat_project').val() ) ;
	}
	
	if ( param('nochrome') != '' ) {
		$('.chrome_autolist').hide() ;
		return ;
	}
	
	$('#query').focus();
} ) ;
